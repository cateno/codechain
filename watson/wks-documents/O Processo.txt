Nome do Filme: O Processo
Data de Lançamento: 17 de maio de 2018
Genero: Documentário
O documentário acompanha a crise política que afeta o Brasil desde 2013 sem nenhum tipo de abordagem direta, como entrevistas ou intervenções nos acontecimentos. A diretora Maria Augusta Ramos passou meses no Planalto e no Congresso Nacional captando imagens sobre votações e discussões que culminaram com a destituição da presidenta Dilma Rousseff do cargo.
