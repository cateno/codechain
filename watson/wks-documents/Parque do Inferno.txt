Nome do Filme: Parque do Inferno
Data de Lançamento: 22 de novembro de 2018
Genero: Terror
Durante a noite de Halloween, um grupo de amigos começa a ser perseguido por um assassino mascarado em um parque de diversões temático. O mais terrível é que todas as atrocidades cometidas pelo criminoso são praticadas na frente do público alienado presente no local. Eles acreditam que tudo faz parte do "show", ignorando os pedidos de socorro dos jovens.
