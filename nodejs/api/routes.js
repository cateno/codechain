'use strict';

var featureFlagController = require('./feature-flag-controller');

module.exports = function(app) {

    //user routes
    app.route('/feature-flag/email').get(featureFlagController.enviaEmail);
    app.route('/feature-flag/sms').get(featureFlagController.enviaSMS);
    app.route('/feature-flag/wa').get(featureFlagController.enviaWhatsApp);
    app.route('/feature-flag/telegram').get(featureFlagController.enviaTelegram);
    app.route('/feature-flag/slack').get(featureFlagController.postSlack);

};