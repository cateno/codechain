'use strict';

var _config = require('../security/config')

var controllers = {

    enviaEmail: async function (req, res) {

        let user;

        const runEnviaEmail = async () => {

            try {

                console.log(' -- envia e-mail');

                let isEnvioEmailEnabled = _config.unleash.isEnabled('F001-EnvioEmail');

                if (isEnvioEmailEnabled){
                    res.status(200).send({msg: 'email enviado'});
                }else{
                    res.status(400).send({msg: 'envio de e-mail desligado'});
                }

            } catch (error) {
                 console.error('falha fatal', error)
                _config.formataError(error, res)
            }
        };

        runEnviaEmail();
        
    },
    enviaSMS: async function (req, res) {

        const runEnviaSMS = async () => {

            try {

                console.log(' -- envia SMS');

                let isEnvioSMSEnabled = _config.unleash.isEnabled('F002-EnvioSMS');

                if (isEnvioSMSEnabled){
                    res.status(200).send({msg: 'SMS enviado'});
                }else{
                    res.status(400).send({msg: 'envio de SMS desligado'});
                }

            } catch (error) {
                 console.error('falha fatal', error)
                _config.formataError(error, res)
            }
        };

        runEnviaSMS();

    },
    enviaWhatsApp: async function (req, res) {

        let user;

        const runEnviaWhatsApp = async () => {

            try {

                console.log(' -- envia whatsapp');

                const context = {
                    userId: 'cateno@codechain.com.br',
                };

                let isEnvioWAEnabled = _config.unleash.isEnabled('F003-EnvioWhatsApp', context);

                if (isEnvioWAEnabled){
                    res.status(200).send({msg: 'whatsapp enviado'});
                }else{
                    res.status(400).send({msg: 'envio de whatsapp desligado'});
                }

            } catch (error) {
                 console.error('falha fatal', error)
                _config.formataError(error, res)
            }
        };

        runEnviaWhatsApp();
        
    },
    enviaTelegram: async function (req, res) {

        let user;

        const runEnviaTelegram = async () => {

            try {

                console.log(' -- envia telegram');
                console.log(' -- IP INTERNET: ' + req.connection.remoteAddress);
                console.log(' -- IP SERVER EM PROXY: ' + req.headers['x-forwarded-for']);
                console.log(' -- IP SOCKET: ' + req.socket.remoteAddress);

                const context = {
                    remoteAddress: req.connection.remoteAddress,
                };

                console.log('context: ' , context)

                let isEnvioWAEnabled = _config.unleash.isEnabled('F004-EnviaTelegram', context);

                if (isEnvioWAEnabled){
                    res.status(200).send({msg: 'telegram enviado'});
                }else{
                    res.status(400).send({msg: 'envio de telegram desligado'});
                }

            } catch (error) {
                 console.error('falha fatal', error)
                _config.formataError(error, res)
            }
        };

        runEnviaTelegram();
        
    },
    postSlack: async function (req, res) {

        let user;

        const runPostSlack = async () => {

            try {

                console.log(' -- envia slack');

                const context = {
                    userId: 'cateno@codechain.com.br',
                };

                console.log('context: ' , context)

                let isPostSlackEnabled = _config.unleash.isEnabled('F005-SlackPost', context);

                if (isPostSlackEnabled){
                    res.status(200).send({msg: 'slack enviado'});
                }else{
                    res.status(400).send({msg: 'post de slack desligado'});
                }

            } catch (error) {
                 console.error('falha fatal', error)
                _config.formataError(error, res)
            }
        };

        runPostSlack();
        
    },
};

module.exports = controllers;