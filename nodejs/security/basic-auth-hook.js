'use strict';

const auth = require('basic-auth');


function basicAuthentication(app) {
    
    app.use('/api/admin/', (req, res, next) => {
        const credentials = auth(req);

        if (credentials) {
            // aqui você deve validar a credential informada
            console.log('credential', credentials)
            next();
        } else {
            return res.status('401').set({ 'WWW-Authenticate': 'Basic realm="codechain"' }).end('access denied');
        }
    });

    app.use((req, res, next) => {
        // Updates active sessions every hour
        req.session.nowInHours = Math.floor(Date.now() / 3600e3);
        next();
    });
}

module.exports = basicAuthentication;