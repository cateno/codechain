const { Pool, Client } = require('pg');
const unleash = require('unleash-server');
const { initialize } = require('unleash-client');
const { hostname } = require('os');
const basicAuthentication = require('./basic-auth-hook');

require('custom-env').env('staging')

var unleashConnectionString = 'postgres://' + process.env.DB_USER + ':' + process.env.DB_PASS;
unleashConnectionString = unleashConnectionString + '@' + process.env.DB_HOST
unleashConnectionString = unleashConnectionString + ':' + process.env.DB_PORT
unleashConnectionString = unleashConnectionString + '/' + process.env.DB_SCHEMA

unleash.start({
    databaseUrl: unleashConnectionString,
    port: 4242,
    secret: 'super-duper-secret',
    adminAuthentication: 'custom',
    preRouterHook: basicAuthentication,
  }).then(unleash => {
    
    console.log(`Unleash started on http://localhost:${unleash.app.get('port')}`,);

    const instance = initialize({
        url: 'http://localhost:4242/api',
        appName: 'codechain',
        instanceId: 'codechain-001',
        refreshInterval: 5000
    });
    
     // optional events
     instance.on('error', console.error);
     instance.on('warn', console.warn);
     instance.on('ready', console.log);
    
     // metrics hooks
     instance.on('registered', clientData => {
         
        console.log('conectado no cliente', clientData)
    
        exports.unleash =  {
            isEnabled,
            getVariant,
            getFeatureToggleDefinition,
            getFeatureToggleDefinitions,
        } = require('unleash-client');
        
        console.log('checando status das features');
        isEnabled('F001-EnvioEmail')
        isEnabled('F002-EnvioSMS')
    
         
     });
    
     instance.on('sent', payload => console.log('metrics bucket/payload sent', payload));
     instance.on('count', (name, enabled) => console.log(`isEnabled(${name}) returned ${enabled}`));

});

console.log()
console.log()
console.log()
console.log('#########################')
console.log('LOADING CONFIG')
console.log('#########################')
console.log()
console.log('Hostname:' + hostname())
console.log('Ambiente:' + process.env.APP_ENV)
console.log('Nome do APP:' + process.env.APP_NAME)
console.log('URL Backend:' + process.env.BACKEND_URL)
console.log('URL WebSite:' + process.env.WEBSITE_URL)
console.log('------------------------')
console.log('Database Host:' + process.env.DB_HOST)
console.log('Database User:' + process.env.DB_USER)
console.log('Database Password:' + process.env.DB_PASS)
console.log('Database Port:' + process.env.DB_PORT)
console.log('Database Schemma:' + process.env.DB_SCHEMA)
console.log('------------------------')
console.log('JWT Issuer:' + process.env.JWT_ISSUER)
console.log('JWT Audience:' + process.env.JWT_AUDIENCE)
console.log('------------------------')
console.log('Email From:' + process.env.EMAIL_FROM)
console.log('Email login:' + process.env.EMAIL_LOGIN)
console.log('Email password:' + process.env.EMAIL_PASSWORD)
console.log('------------------------')
console.log('AWS KEY:' + process.env.AWS_KEY)
console.log('AWS SECRET:' + process.env.AWS_SECRET)
console.log('AWS S3 UPLOAD BUCKET:' + process.env.S3_BUCKET_UPLOAD)
console.log('------------------------')
console.log()
console.log()

exports.backend_url = process.env.BACKEND_URL;
exports.website_url = process.env.WEBSITE_URL;

exports.db_host = process.env.DB_HOST;
exports.db_user = process.env.DB_USER;
exports.db_password = process.env.DB_PASS;
exports.db_port = process.env.DB_PORT;
exports.db_schemma = process.env.DB_SCHEMA;
exports.jwt_issuer = process.env.JWT_ISSUER;
exports.jwt_audience = process.env.JWT_AUDIENCE;

exports.email_from = process.env.EMAIL_FROM;
exports.email_login = process.env.EMAIL_LOGIN;
exports.email_password = process.env.EMAIL_PASSWORD;

exports.s3_bucket_upload = process.env.S3_BUCKET_UPLOAD;
exports.aws_key = process.env.AWS_KEY;
exports.aws_secret = process.env.AWS_SECRET;

exports.pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_SCHEMA,
    password: process.env.DB_PASS,
    port: process.env.DB_PORT,
})

exports.formataError = function (error, response) {

    console.log('error', error);

    if (isNaN(error)) {
        response.status(500).send({
            erro: 'Falha Fatal!',
            detail: error
        });
    } else {

        var detail = null;

        if (error == 401)
            msg = 'acesso não permitido';

        if (error == 404)
            msg = 'registro não encontrado';

        response.status(error).send({
            erro: msg
        });
    }
}

