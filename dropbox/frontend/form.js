let backendAPI = "http://localhost:8081/dropbox/uploadFile"

$(function() {
  function after_form_submitted(data) {

    console.log("result", result)
    if (data.result == "success") {
      $("form#formDropbox").hide();
      $("#success_message").show();
      $("#error_message").hide();
    } else {
      $("#error_message").append("<ul></ul>");

      jQuery.each(data.errors, function(key, val) {
        $("#error_message ul").append("<li>" + key + ":" + val + "</li>");
      });
      $("#success_message").hide();
      $("#error_message").show();

      //reverse the response on the button
      $('button[type="button"]', $form).each(function() {
        $btn = $(this);
        label = $btn.prop("orig_label");
        if (label) {
          $btn.prop("type", "submit");
          $btn.text(label);
          $btn.prop("orig_label", "");
        }
      });
    } //else
  }

  $("#formDropbox").submit(function(e) {
    e.preventDefault();

    $form = $(this);
    //show some response on the button
    $('button[type="submit"]', $form).each(function() {
      $btn = $(this);
      $btn.prop("type", "button");
      $btn.prop("orig_label", $btn.text());
      $btn.text("Sending ...");
    });

    var nomeArquivo = $("#file").val();
    nomeArquivo = nomeArquivo.split("\\")[2];

    console.log('nome informado', $("#name").val());
    console.log('arquivo informado', nomeArquivo);

    var formdata = new FormData(this);

    var data = {
        path: "/folder/" + nomeArquivo,
        mode: "add",
        autorename : true,
        mute : false,
        strict_conflict : false
    };


    $.ajax({
      type: "POST",
      url: backendAPI,
      beforeSend: function(request) {
        request.setRequestHeader("Dropbox-API-Arg", JSON.stringify(data));
      },
      data: formdata,
      success: after_form_submitted,
      processData: false,
      contentType: false,
      cache: false
    });
  });
});
