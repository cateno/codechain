package br.com.triscal.ia.xp.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.triscal.ia.xp.service.DropBoxService;

@RestController
@RequestMapping("/dropbox")
public class DropboxController {

	private final DropBoxService dropBoxService;

	@Autowired
	public DropboxController(final DropBoxService dropBoxService) {
		this.dropBoxService = dropBoxService;
	}

	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public String uploadFile(@RequestParam("file") MultipartFile file, @RequestHeader("Dropbox-API-Arg") final String dropboxAPIArg) {

		try {

			String fileName = dropBoxService.storeFile(file);
			
			URL url = new URL("https://content.dropboxapi.com/2/files/upload");
		    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		    
		    conn.setDoOutput(true);
		    conn.setConnectTimeout(60000); 
		    conn.setRequestMethod("POST");
		    conn.setRequestProperty("Authorization", "Bearer zm-nQGAKnMAAAAAAAAAQdHTuOmPJU4DixkWUcwCY3MxqmAY02qqU6M5QWNuHxcJ8");
		    conn.setRequestProperty("Dropbox-API-Arg", dropboxAPIArg);
		    conn.setRequestProperty("Content-Type", MediaType.APPLICATION_OCTET_STREAM_VALUE);
		    
		    conn.connect();
		    conn.getOutputStream().write(file.getBytes());
		    conn.getOutputStream().flush();
		    
		    int status = conn.getResponseCode();
		    
		    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line+"\n");
            }
            br.close();
            return sb.toString();

		} catch (Exception e) {
			return null;
		}
	}

	@GetMapping("/downloadFile/{fileName:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {

		try {
			// Load file as Resource
			Resource resource = dropBoxService.loadFileAsResource(fileName);

			// Try to determine file's content type
			String contentType = null;
			try {
				contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
			} catch (IOException ex) {

			}

			// Fallback to the default content type if type could not be determined
			if (contentType == null) {
				contentType = "application/octet-stream";
			}

			return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
					.body(resource);

		} catch (Exception e) {

		}
		return null;
	}

}
